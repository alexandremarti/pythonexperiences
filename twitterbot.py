
import tweepy
import argparse

parser = argparse.ArgumentParser(description="My twitter bot")

parser.add_argument('-t', action='store', dest='t')
parser.add_argument('-rt', action='store', dest='rt')
parser.add_argument('-tl', action="store_true")
parser.add_argument("--nr", action='store', dest='nr', default=10, type=int)  # number of tweets to list
parser.add_argument('-ts', action="store_true")
parser.add_argument("--tx", action="store", dest='tx', default = "")


# Access apps.twitter.com, create your app to generate your keys
consumer_key="<PUT YOUR INFORMATION HERE>"
consumer_secret="<PUT YOUR INFORMATION HERE>"

access_token="<PUT YOUR INFORMATION HERE>"
access_token_secret="<PUT YOUR INFORMATION HERE>"

auth = tweepy.OAuthHandler(consumer_key,consumer_secret)
auth.set_access_token(access_token,access_token_secret)



api = tweepy.API(auth, wait_on_rate_limit=True)
my_user = api.me()

args = parser.parse_args()

if args.tl:
    for cnt, tweet in enumerate(api.home_timeline(count=args.nr)):
        print("Tweet {}".format(cnt+1))
        print("Id used to Retweet: {}".format(tweet.id))
        print("User Screen Name: {}".format(tweet.user.screen_name))
        print("Tweet:")
        print(tweet.text)                
        print("\n")
if args.t:
    api.update_status(args.t)
    print("Tweet postado!")

if args.rt:
    api.retweet(args.rt)
    print("Retweet postado!")

if args.ts:
    if args.tx == "":
        print("Ops, I need some text to search!!")
        print("Ex: -ts --tx='#python'")
    else:    
        for cnt, tweet in enumerate(tweepy.Cursor(api.search, q=args.tx).items(args.nr)):
            print("Tweet {}".format(cnt+1))
            print("Id used to Retweet: {}".format(tweet.id))
            print("User Screen Name: {}".format(tweet.user.screen_name))
            print("Tweet:")
            print(tweet.text)                
            print("\n")